//
//  NetworkTest.m
//  NetworkTest
//
//  Created by P.I.akura on 2012/12/25.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#import "NetworkTest.h"
#include "AHBP_Func.h"

@implementation NetworkTest
bool equalsVI(vector<int> vi1,vector<int> vi2) {
    int _len = (int)vi1.size();
    if (_len != vi2.size()) return false;
    
    for (int i=0;i<_len;i++){
        if (vi1[i] != vi2[i])
            return false;
    }
    return true;
}

bool equalsN(neighbor n1,neighbor n2) {
    return n1.nodeNum == n2.nodeNum && equalsVI(n1.adjacent, n2.adjacent);
}
bool equalsVN(vector<neighbor> vn1,vector<neighbor> vn2) {
    int _len = (int)vn1.size();
    if (_len != vn2.size()) return false;
    
    for (int i=0;i<_len;i++){
        if (!equalsN(vn1[i], vn2[i]))
            return false;
    }
    return true;
}

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}
- (void)testExample
{
    vector<int> n1 = {1};
    vector<int> n2;
    n2.push_back(1);
    
    NSAssert(equalsVI(n1, n2), @"s");
}
- (void)testOfEqualsN
{
    neighbor n1 = {1, (vector<int>){1, 2, 3}};
    neighbor n2;
    n2.nodeNum = 1;
    n2.adjacent.push_back(1);
    n2.adjacent.push_back(2);
    n2.adjacent.push_back(3);
    
    NSAssert(equalsN(n1, n2) , @"s");
    n2.adjacent.push_back(4);
    NSAssert(!equalsN(n1, n2) , @"s");
}
- (void)testOfEqualsVN
{
    vector<neighbor> vn1 = {(neighbor){1, (vector<int>){1, 2, 3}},(neighbor){2, (vector<int>){4, 5, 6}}};
    vector<neighbor> vn2;
    vn2.push_back((neighbor){1, (vector<int>){1, 2, 3}});
    vn2.push_back((neighbor){2, (vector<int>){4, 5, 6}});
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
    vn2.pop_back();
    vn2.push_back((neighbor){2, (vector<int>){4, 5, 7}});
    NSAssert(!equalsVN(vn1, vn2) , @"s");
}
- (void)testCreateDominater {
    vector<int> n1 = createDominator({neighbor{1,vector<int>{2}}}, {}, 0);
    vector<int> n2 = {1};
    
    NSAssert(equalsVI(n1, n2), @"s");
}
- (void)testDeleteN1Route
{
    vector<neighbor> vn = {(neighbor){1, (vector<int>){2, 4, 6}},(neighbor){2, (vector<int>){1, 5}}, (neighbor){3, (vector<int>){7, 8, 9}}, (neighbor){4, (vector<int>){7, 8, 9}}};
    vector<neighbor> vn1 = deleteN1Route({1, 2}, vn);
    vector<neighbor> vn2 = {(neighbor){3, (vector<int>){7, 8, 9}}};
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
}
- (void)testDeleteN1
{
    vector<neighbor> vn = {(neighbor){1, (vector<int>){1, 2, 3}},(neighbor){2, (vector<int>){4, 5, 6}}};
    vector<neighbor> vn1 = deleteN1(vn, 1);
    vector<neighbor> vn2 = {(neighbor){2, (vector<int>){4, 5, 6}}};
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
}
- (void)testDeleteN2
{
    vector<neighbor> vn = {(neighbor){1, (vector<int>){1, 2, 3}},(neighbor){2, (vector<int>){4, 5, 6}}};
    vector<neighbor> vn1 = deleteN2(vn, 1);
    vector<neighbor> vn2 = {(neighbor){1, (vector<int>){2, 3}},(neighbor){2, (vector<int>){4, 5, 6}}};
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
}
- (void)testdeleteEdgeOfNeifV {
    vector<neighbor> n1 = deleteEdgeOfNeifV({neighbor{1, vector<int>{2, 3}}, neighbor{2, vector<int>{1, 3}}, neighbor{3, vector<int>{1, 2}}});
    vector<neighbor> n2 = {neighbor{1, vector<int>{}}, neighbor{2, vector<int>{}}, neighbor{3, vector<int>{}}};
    
    NSAssert(equalsVN(n1, n2), @"s");
}
- (void)testDeleteN2List
{
    vector<neighbor> vn = {(neighbor){1, (vector<int>){1, 2, 3}},(neighbor){2, (vector<int>){4, 5, 6}}};
    vector<neighbor> vn1 = deleteN2(vn, {1, 4});
    vector<neighbor> vn2 = {(neighbor){1, (vector<int>){2, 3}},(neighbor){2, (vector<int>){5, 6}}};
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
}
- (void)testDeleteNode2
{
    vector<neighbor> vn = {(neighbor){1, (vector<int>){4, 5, 6}},(neighbor){2, (vector<int>){1, 2, 3}}};
    vector<neighbor> vn1 = deleteNodes(vn, {(neighbor){2, (vector<int>){1}}});
    vector<neighbor> vn2 = {(neighbor){2, (vector<int>){2, 3}}};
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
}
- (void)testDeleteN2Route
{
    vector<neighbor> vn = {(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){7, 9, 5}}, (neighbor){3, (vector<int>){4, 5, 6}}};
    vector<neighbor> vn1 = deleteN2Route({4}, vn);
    vector<neighbor> vn2 = {(neighbor){2, (vector<int>){7, 9, 5}}};
    
    NSAssert(equalsVN(vn1, vn2) , @"s");
    
    vn1 = deleteN2Route({}, {neighbor{2, vector<int>{7, 9, 5}}});
    NSAssert(equalsVN(vn1, vn2),@"s2");
}
- (void)testMakeDominator {
    vector<int> vi1 = makeDominator({(neighbor){1, (vector<int>){8, 4, 6}}});
    vector<int> vi2 = {1};
    
    NSAssert(equalsVI(vi1, vi2), @"s");
    
    vi1 = makeDominator({(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){8, 4, 6}}});
    vi2 = {1};
    
    NSAssert(equalsVI(vi1, vi2), @"s");
}
- (void)testEmptyAdjacent {
    NSAssert(emptyAdjacent({}), @"s2");
    NSAssert(emptyAdjacent({(neighbor){1, (vector<int>){8, 4, 6}}}) == false, @"s2");
    NSAssert(emptyAdjacent({(neighbor){1, (vector<int>){}}}), @"s2");
}
- (void)testSelectDom
{
    int i1 = selectDom({(neighbor){1, (vector<int>){8, 4, 6}}});
    int i2 = 1;
    
    NSAssert(i1 == i2, @"s");
}
- (void)testSelectDomOfMax
{
    int i1 = selectDomOfMax({(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){7, 9, 10, 11}}});
    int i2 = 2;
    
    NSAssert(i1 == i2, @"s");
}
- (void)testSelectDomOfSingleN2
{
    int i1 = selectDomOfSingleN2({(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){4, 6}}});
    int i2 = 1;
    
    NSAssert(i1 == i2, @"s");
}
- (void)testExistNN
{
    NSAssert(existNN({(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){7, 9}}}, 1) == 0, @"s");
    NSAssert(existNN({(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){7, 9}}}, 2) == 1, @"s");
    NSAssert(existNN({(neighbor){1, (vector<int>){8, 4, 6}},(neighbor){2, (vector<int>){7, 9}}}, 4) == -1, @"s");
}
- (void)testPushNs
{
    vector<neighbor> vn;
    NSAssert(equalsVN(vn, {}), @"pushNs :: 1");
    vn = pushNs(vn, 1, 4);
    NSAssert(equalsVN(vn, {(neighbor){1, (vector<int>){4}}}), @"pushNs :: 6");
    vn = pushNs(vn, 2, 7);
    NSAssert(equalsVN(vn, {(neighbor){1, (vector<int>){4}},(neighbor){2, (vector<int>){7}}}), @"pushNs :: 6");
    vn = pushNs(vn, 1, 6);
    NSAssert(equalsVN(vn, {(neighbor){1, (vector<int>){4, 6}},(neighbor){2, (vector<int>){7}}}), @"pushNs :: 6");
    vn = pushNs(vn, 2, 9);
    NSAssert(equalsVN(vn, {(neighbor){1, (vector<int>){4, 6}},(neighbor){2, (vector<int>){7, 9}}}), @"pushNs :: 6");
    vn = pushNs(vn, 1, 8);
    NSAssert(equalsVN(vn, {(neighbor){1, (vector<int>){4, 6, 8}},(neighbor){2, (vector<int>){7, 9}}}), @"pushNs :: 6");
}
@end
