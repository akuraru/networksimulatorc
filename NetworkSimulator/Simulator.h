//
//  NodeContainer.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__Simulator__
#define __NetworkSimulator__Simulator__

#include "NodeContainer.h"
#include "LinkContainer.h"

class Simulator {
    double const TransferRange = 50.0;
    
    virtual int nodeNum() = 0;
    virtual Mobility mobility(int) = 0;
    virtual int dataRate() = 0;
    
    // 転送関連
    virtual double transferRate(int nodeId) = 0;
    virtual double transmissionDistance() = 0;
    virtual double transmissionDelay() = 0;
    
    virtual int containerNum() = 0;
    virtual ApplicationContainer applicationContainer(int) = 0;
    virtual double helloInterval() = 0;
    virtual int helloPacketSize() = 0;
    
    NodeContainer nc;
    LinkContainer lc;
    
    bool existEvent();
    Tuple<int, int> minNextEvent();
    
public :
    void run();
    string toString();
};

#endif /* defined(__NetworkSimulator__NodeContainer__) */
