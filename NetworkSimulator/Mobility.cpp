//
//  Mobility.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#include "Mobility.h"
#include "math.h"

Mobility::Mobility() {
    _position = Vector{0.0, 0.0, 0.0};
    _model = (MobilityModel)0;
}
Mobility::Mobility(Vector p, MobilityModel mm) {
    _position = p;
    _model = mm;
}
void Mobility::setPosition(Vector p) {
    _position = p;
}
Vector Mobility::postion() {
    return _position;
}
void Mobility::setModel(MobilityModel mm) {
    _model = mm;
}
MobilityModel Mobility::model() {
    return _model;
}
double Mobility::nearsight(Mobility m) {
    double dis = sqrt(
    pow(_position.x - m.postion().x, 2.0) +
    pow(_position.y - m.postion().y, 2.0) +
    pow(_position.z - m.postion().z, 2.0));
    
    return (0 < dis)? dis : MAXFLOAT ;
}