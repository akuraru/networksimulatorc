//
//  File.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/08.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#include "LinkContainer.h"

bool comper(const Link& l1, const Link& l2) {
    return l1._event.StartTime < l2._event.StartTime;
}

void LinkContainer::addEvent(vector<Link> ls) {
    for (int i=0, _len=(int)ls.size();i <_len; i++) {
        links.push_back(ls[i]);
    }
    
    sort(links.begin(), links.end(), comper);
}
bool LinkContainer::existLinkEvent() {
    int _linkNum = (int)links.size();
    for (int i = 0; i < _linkNum; i++) {
        if (1 < links[i]._event.EndNodeId)
            return true;
    }
    return false;
}
Tuple<double, int> LinkContainer::minLinkNextEvent() {
    int _linkNum = (int)links.size();
    double _min = INT_MAX;
    double _minNum = -1;
    for (int i = 0; i < _linkNum; i++) {
        double tna = links[i].timeNextApplication();
        if (tna != -1) {
            if (tna < _min ) {
                _min = tna;
                _minNum = i;
            }
        }
    }
    return Tuple<double, int>(_min, _minNum);
}
bool LinkContainer::existCollisionLink(int i) {
    int nn = links[i].nextNode();
    for (int j = 0, _len = (int)links.size(); j < _len; j++) {
        if (i != j) {
            bool b = links[j].nextNode() == nn;
            links[j].setCollicion(b);
            links[i].setCollicion(b);
        }
    }
    return links[i].collision();
}

Tuple<int, ApplicationContainer> LinkContainer::linkEvent(int i) {
    Tuple<int, ApplicationContainer> result(-1, links[i].event());
    if (!existCollisionLink(i)) {
        result._1 = links[i].nextNode();
    }
    links.erase(links.begin() + i);
    return result;
}