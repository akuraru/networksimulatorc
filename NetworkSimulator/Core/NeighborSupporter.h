//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/09.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__NeighborSupporter__
#define __NetworkSimulator__NeighborSupporter__

#include <vector>
#include "Struct.h"
#include "Neighbor.h"

using namespace std;

class NeighborSupporter {
    vector<neighbor> neighbors;
public:
    void setNeighbor(Neighbor*);
    Neighbor* getAdjacent();
    vector<neighbor> getNeighbor();
    
    string toString();
};

#endif /* defined(__NetworkSimulator__File__) */
