//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/24.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__Link__
#define __NetworkSimulator__Link__

#include "vector"
#include "Struct.h"

using namespace std;

class Link {
    int _nextNode;
    bool isCollision;
public:
    ApplicationContainer _event;
    
    void setCollicion(bool);
    bool collision();
    Link(ApplicationContainer, double, int);
    double timeNextApplication();
    ApplicationContainer event();
    int nextNode();
};

#endif /* defined(__NetworkSimulator__File__) */
