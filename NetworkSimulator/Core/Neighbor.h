//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/11.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__Neighbor__
#define __NetworkSimulator__Neighbor__

#include "Struct.h"
#include <vector>

using namespace std;

typedef struct{
    int nodeNum;
    vector<int> adjacent;
}neighbor;

class Neighbor : Option {
    neighbor n;
public:
    Neighbor(neighbor);
    neighbor getNeighbor();
    void setNodeNum(int);
};

#endif /* defined(__NetworkSimulator__File__) */
