//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/09.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__HelloPacket__
#define __NetworkSimulator__HelloPacket__

#include "float.h"
#include "Struct.h"

class HelloPacket {
    double interval;
    int packetSize;
    double _nextTime = DBL_MAX;
public:
    void init(double, int);
    double nextTime();
    void delay(double);
    void helloDelay();
    ApplicationContainer helloPacket();
};
#endif /* defined(__NetworkSimulator__File__) */
