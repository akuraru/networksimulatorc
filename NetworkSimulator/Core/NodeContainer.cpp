//
//  File.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/08.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#include "NodeContainer.h"
#include <sstream>

void NodeContainer::initNodes(int nodeNum,Delegate<Mobility, int> mobility,Delegate<double, int>tr) {
    _nodeNum = nodeNum;
    nodes.reserve(nodeNum);
    
    for (int i = 0; i < nodeNum ; i++ ) {
        Node n;
        nodes[i] = n;
        nodes[i].setNodeNum(i);
        nodes[i].setMobility(mobility.f(i));
        nodes[i].setTransferRate(tr.f(i));
    }
}
void NodeContainer::initAC(int cNum, Delegate<ApplicationContainer, int> applicationContainer) {
    for (int i = 0; i < cNum; i++) {
        ApplicationContainer ac = applicationContainer.f(i);
        nodes[ac.StartID].addEvent(ac);
    }
}

void NodeContainer::initHello(double interval, int size) {
    for (int i = 0; i < _nodeNum; i++) {
        nodes[i].initHello(interval, size);
    }
}

bool NodeContainer::existNodeEvent() {
    for (int i = 0; i < _nodeNum; i++) {
        if (nodes[i].existEvent() == true)
            return true;
    }
    return false;
}

Tuple<double, int> NodeContainer::minNodeNextEvent() {
    double _min = INT_MAX;
    int _minNum = -1;
    for (int i = 0; i < _nodeNum; i++) {
        double tna = nodes[i].timeNextApplication();
        if (tna != -1) {
            if (tna < _min ) {
                _min = tna;
                _minNum = i;
            }
        }
    }
    return Tuple<double, int>(_min, _minNum);
}

vector<Link> NodeContainer::nodeEvent(int i,double transmissionDistance) {
    vector<Link> links;
    for (int j=0  ; j < _nodeNum ; j++ ) {
        if (nodes[i].transfer(nodes[j], transmissionDistance)) {
            ApplicationContainer ac = nodes[i].event();
            Link l(ac, nodes[i].TransferRate(), j);
            nodes[j].delay(ac.StartTime, l.timeNextApplication());
            links.push_back(l);
        }
    }
    nodes[i].stepEvent();
    return links;
}
void NodeContainer::addEvent(Tuple<int,ApplicationContainer> t, double transmission) {
    if (t._1 != -1) {
        nodes[t._1].setEvent(t._2, transmission);
    }
}

string NodeContainer::toString() {
    stringstream str;
    for (int i = 0, _len=_nodeNum; i < _len; i++) {
        str << "Node" << i << endl << nodes[i].toString() << endl;
    }
    return str.str();
}