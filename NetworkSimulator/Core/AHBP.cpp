//
//  File.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/11.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#include "AHBP.h"
#include "AHBP_Func.h"

AHBP::AHBP(AHBP* ahbp, vector<neighbor> neig, int nodeNum) {
    route = ahbp->getRoute();
    dominator = createDominator(neig, route, nodeNum);
}

AHBP::AHBP(vector<int> d,vector<int> r) {
    dominator = d;
    route = r;
}
AHBP::AHBP(const AHBP* ahbp) {
    route = ahbp->route;
    dominator = ahbp->dominator;
}
vector<int> AHBP::getRoute() {
    return route;
}
vector<int> AHBP::getDominator() {
    return dominator;
}

bool AHBP::isDominator(int n) {
    for (int i=0,_len=(int)dominator.size();i<_len;i++) {
        if (dominator[i] == n) {
            return true;
        }
    }
    return false;
}
void AHBP::setNodeNum(int num) {
    route.push_back(num);
}