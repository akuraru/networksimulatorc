//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/08.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__delegate__
#define __NetworkSimulator__delegate__

template<typename rv_t, typename arg_t> class Delegate{
    typedef Delegate<rv_t, arg_t> this_t;
    void *obj,*method;
    
    template<typename obj_t> static rv_t __f(arg_t a, this_t *this_obj){
        typedef rv_t(obj_t::*method_t)(arg_t);
        method_t method = *(method_t*)this_obj->method;
        obj_t *obj = (obj_t*)this_obj->obj;
        return (obj->*method)(a);
    }
    
    rv_t(*_f)(arg_t, this_t*);
    
public:
    rv_t f(arg_t a){ return _f(a, this); }
    
    template<typename obj_t> void assign(obj_t *o, rv_t(obj_t::*m)(arg_t)){
        typedef rv_t(obj_t::*method_t)(arg_t);
        obj = (void*)o;
        delete (method_t*)method;
        method = (void*)new (method_t*)();
        *(method_t*)method = m;
        _f = __f<obj_t>;
    }
    
    Delegate() : method(0){}
    template<typename obj_t> Delegate(obj_t *o, rv_t(obj_t::*m)(arg_t)) {
        method = 0;
        assign(o, m);
    }
    ~Delegate(){
        typedef rv_t(*method_t)(arg_t);
        delete (method_t*)method;
    }
};
#endif /* defined(__NetworkSimulator__File__) */
