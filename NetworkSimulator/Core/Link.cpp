
//
//  File.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/24.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#include "Link.h"

Link::Link(ApplicationContainer ac, double transRate,  int nn) {
    _event = ac;
    _event.StartTime += _event.data / transRate;
    _nextNode = nn;
    isCollision = false;
}
void Link::setCollicion(bool c) {
    isCollision = isCollision || c;
}
bool Link::collision() {
    return isCollision;
}
double Link::timeNextApplication() {
    return _event.StartTime;
}
ApplicationContainer Link::event() {
    return _event;
}
int Link::nextNode() {
    return _nextNode;
}