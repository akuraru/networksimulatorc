//
//  AHBP_Func.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/13.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//
#include <functional>
#include "AHBP_Func.h"

using namespace std;
using namespace placeholders;

vector<int> createDominator(vector<neighbor> neig,vector<int> route,int nodeNum) {
    neig = deleteN1(neig, nodeNum);
    neig = deleteN1Route(route, neig);
    neig = deleteN2Route(route, neig);
    neig = deleteEdgeOfNeifV(neig);
    return makeDominator(neig);
}
Tuple<vector<neighbor>, vector<neighbor>>sep(vector<int> route, vector<neighbor> neig){
    vector<neighbor> result;
    vector<neighbor> erase;
    for (int j=0,_lenN=(int)neig.size();j<_lenN;j++) {
        if (existVI(route, neig[j].nodeNum)) {
            erase.push_back(neig[j]);
        } else {
            result.push_back(neig[j]);
        }
    }
    return {result, erase};
}
vector<neighbor> deleteN1Route(vector<int> route, vector<neighbor> neig) {
    Tuple<vector<neighbor>, vector<neighbor>> t = sep(route,neig);
    vector<neighbor> result = t._1;
    vector<neighbor> erase = t._2;
    result = deleteNodes(result, erase);
    return deleteN1(result, route);
}
////////////


vector<neighbor> deleteN2Route(vector<int> route, vector<neighbor> neig) {
    vector<neighbor> n;
    for (int j=0,_lenN=(int)neig.size();j<_lenN;j++) {
        if (existVI(neig[j].adjacent, route) == false) {
            n.push_back(neig[j]);
        }
    }
    
    return n;
}
//////////////

vector<neighbor> deleteEdgeOfNeifV(vector<neighbor> vn) {
    return deleteN2(vn, map<neighbor, int>(vn, [](neighbor n){
        return n.nodeNum;
    }));
    
}

//////////////

vector<int> makeDominator(vector<neighbor> n) {
    vector<int> dom;
    while (emptyAdjacent(n) == false) {
        int d = selectDom(n);
        dom.push_back(d);
        n = deleteN1(deleteN2(n, n[existNN(n, d)].adjacent), d);
    }
    return dom;
}

///
template <typename T,typename U>
vector<T> filter(vector<T> source, U option, bool (*comp)(T,U)){
    vector<T> result;
    for (int i=0,_lenN=(int)source.size();i<_lenN;i++) {
        if (comp(source[i],option)) {
            result.push_back(source[i]);
        }
    }
    return result;
}
template <typename T,typename U>
vector<T> map(vector<T> s, U o, T (*f)(T,U)){
    vector<T> v;
    for (T e: s) v.push_back(f(e, o));
    return v;
}
template <typename T,typename U>
vector<U> map(vector<T> s, U (*f)(T)) {
    vector<U> v;
    for (T e: s) v.push_back(f(e));
    return v;
}
template <typename T,typename U>
U foldLeft(vector<T> a, U b, U (*c)(U,T)) {
    for(T e: a) { b = c(b, e); }
    return b;
}
template <class _Fp,class Arg>
inline _LIBCPP_INLINE_VISIBILITY
__bind<_Fp, Arg>
bmic(Arg v,_Fp f) {
    typedef __bind<_Fp, Arg> type;
    return type(_VSTD::forward<_Fp>(f), _VSTD::forward<Arg>(v));
}

///
vector<neighbor> deleteN1(vector<neighbor> n,int nodeNum) {
    return filter<neighbor, int>(n, nodeNum, [](neighbor n,int nodeNum){return nodeNum != n.nodeNum;});
}
vector<neighbor> deleteN2(vector<neighbor> n,int nodeNum) {
    return map<neighbor, int>(n, nodeNum, [](neighbor n, int nodeNum){
        n.adjacent = filter<int, int>(n.adjacent, nodeNum, [](int adj, int nodeNum){
            return adj != nodeNum;
        });
        return n;
    });
}
vector<neighbor> deleteN1(vector<neighbor> n,vector<int> nnList) {
    for (int j=0,_lenN=(int)nnList.size();j<_lenN;j++) {
        n = deleteN1(n, nnList[j]);
    }
    return n;
}
vector<neighbor> deleteN2(vector<neighbor> n,vector<int> nnList) {
    for (int j=0,_lenN=(int)nnList.size();j<_lenN;j++) {
        n = deleteN2(n, nnList[j]);
    }
    return n;
}
vector<neighbor> deleteNodes(vector<neighbor> n,vector<neighbor> a) {
    return bmic(bmic(n,bind(
    foldLeft<neighbor, vector<neighbor>>,a, _1, [](vector<neighbor> n, neighbor a){
        return deleteN2(n, a.adjacent);
    }))(),bind(
    foldLeft<neighbor, vector<neighbor>>,a, _1, [](vector<neighbor> n, neighbor a){
        return deleteN1(n, a.adjacent);
    }))();
}
///
bool existVI(vector<int> a, int nn) {
    for (int k=0,_lenA=(int)a.size();k<_lenA;k++) {
        if (a[k] == nn)
            return true;
        
    }
    return false;
}
bool existVI(vector<int> a, vector<int> vi) {
    for (int k=0,_lenA=(int)a.size();k<_lenA;k++) {
        for (int nn: vi){
            if (a[k] == nn)
                return true;
        }
    }
    return false;
}

///

bool emptyAdjacent(vector<neighbor> n) {
    for (int j=0,_lenN=(int)n.size();j<_lenN;j++) {
        if (0 < n[j].adjacent.size())
            return false;
    }
    return true;
}
int existNN(vector<neighbor> n, int nn) {
    for (int j=0,_lenN=(int)n.size();j<_lenN;j++) {
        if (n[j].nodeNum == nn)
            return j;
    }
    return -1;
}
vector<neighbor> pushNs(vector<neighbor> n, int nn,int a) {
    int eNN = existNN(n, nn);
    if (eNN != -1) {
        n[eNN].adjacent.push_back(a);
    } else {
        vector<int> v;
        v.push_back(a);
        n.push_back({nn, v});
    }
    return n;
}
int selectDomOfSingleN2(vector<neighbor> n) {
    vector<neighbor> n2;
    for (int j=0,_lenN=(int)n.size();j<_lenN;j++) {
        for (int k=0,_lenA=(int)n[j].adjacent.size();k<_lenA;k++) {
            n2 = pushNs(n2, n[j].adjacent[k], n[j].nodeNum);
        }
    }
    for (int j=0,_lenN=(int)n2.size();j<_lenN;j++) {
        if (n2[j].adjacent.size() == 1)
            return n2[j].adjacent[0];
    }
    return -1;
}
int selectDomOfMax(vector<neighbor> n) {
    int nodeNum = n[0].nodeNum;
    int maxAdj = (int)n[0].adjacent.size();
    for (int i=1,_len=(int)n.size();i<_len;i++) {
        if (maxAdj < n[i].adjacent.size()) {
            nodeNum = n[i].nodeNum;
            maxAdj = (int)n[i].adjacent.size();
        }
    }
    return nodeNum;
}
int selectDom(vector<neighbor> n) {
    int d = selectDomOfSingleN2(n);
    return (d != -1)? d : selectDomOfMax(n);
}
