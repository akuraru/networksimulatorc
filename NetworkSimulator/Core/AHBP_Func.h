//
//  AHBP_Func.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/13.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__AHBP_Func__
#define __NetworkSimulator__AHBP_Func__

#include "Neighbor.h"
#include <vector>

using namespace std;

vector<int> createDominator(vector<neighbor>,vector<int>,int);
vector<neighbor> deleteN1Route(vector<int>, vector<neighbor>);
vector<neighbor> deleteN2Route(vector<int>, vector<neighbor>);
vector<neighbor> deleteEdgeOfNeifV(vector<neighbor>);
vector<int> makeDominator(vector<neighbor>);

///
vector<neighbor> deleteN1(vector<neighbor> ,int);
vector<neighbor> deleteN1(vector<neighbor> ,vector<int>);
vector<neighbor> deleteN2(vector<neighbor> ,int);
vector<neighbor> deleteN2(vector<neighbor> ,vector<int>);
vector<neighbor> deleteNodes(vector<neighbor>,vector<neighbor>);
///
bool existVI(vector<int> a, int nn);
bool existVI(vector<int> a, vector<int> vi);
///
bool emptyAdjacent(vector<neighbor>);
int selectDom(vector<neighbor>);
int existNN(vector<neighbor>,int);
vector<neighbor> pushNs(vector<neighbor>,int,int);
int selectDomOfSingleN2(vector<neighbor>);
int selectDomOfMax(vector<neighbor>);


template <typename T,typename U>
vector<U> map(vector<T> s, U (*f)(T));

#endif /* defined(__NetworkSimulator__AHBP_Func__) */
