//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/11.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__AHBP__
#define __NetworkSimulator__AHBP__

#include "Struct.h"
#include "Neighbor.h"
#include <vector>

using namespace std;

class AHBP : Option {
    vector<int> dominator;
    vector<int> route;
public:
    AHBP(AHBP*, vector<neighbor>, int nodeNum);
    AHBP(vector<int>, vector<int>);
    AHBP(const AHBP*);
    AHBP(){};
    vector<int> getRoute();
    vector<int> getDominator();
    bool isDominator(int);
    void setNodeNum(int);
};

#endif /* defined(__NetworkSimulator__File__) */
