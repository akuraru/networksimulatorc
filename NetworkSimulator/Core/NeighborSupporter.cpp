//
//  File.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/09.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#include "NeighborSupporter.h"
#include <sstream>

void NeighborSupporter::setNeighbor(Neighbor *N) {
    neighbor n = N->getNeighbor();
    int _len = (int)neighbors.size();
    for (int i = 0; i < _len ; i++) {
        if(n.nodeNum == neighbors[i].nodeNum) {
            neighbors.erase(neighbors.begin() + i);
        }
    }
    neighbors.push_back(n);
}

Neighbor* NeighborSupporter::getAdjacent() {
    neighbor n;
    n.nodeNum = -1;
    int _len = (int)neighbors.size();
    for (int i = 0; i < _len ; i++) {
        n.adjacent.push_back(neighbors[i].nodeNum);
    }
    return new Neighbor(n);
}
vector<neighbor> NeighborSupporter::getNeighbor() {
    return neighbors;
}
string NeighborSupporter::toString() {
    stringstream str;
    for (int i=0,_len = (int)neighbors.size();i<_len;i++) {
        str << "n -> " << neighbors[i].nodeNum;
        for (int j=0,_len2=(int)neighbors[i].adjacent.size();j<_len2;j++) {
            str << " :: " << neighbors[i].adjacent[j];
        }
        str << "\n";
    }
    
    return str.str();
}