//
//  File.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/09.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#include "HelloPacket.h"
#include <stdlib.h>

void HelloPacket::init(double i, int ps) {
    interval = i;
    packetSize = ps;
    _nextTime = interval *(1 + ((double)rand())/RAND_MAX);
}
double HelloPacket::nextTime() {
    return _nextTime;
}
void HelloPacket::delay(double d) {
    _nextTime += d;
}
void HelloPacket::helloDelay() {
    _nextTime += interval *(1 + ((double)rand())/RAND_MAX);
}

ApplicationContainer HelloPacket::helloPacket() {
    ApplicationContainer ac = {0,1,_nextTime,_nextTime+250, packetSize, -1};
    return ac;
}
