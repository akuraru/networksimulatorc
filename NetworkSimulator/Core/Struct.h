//
//  Struct.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#ifndef NetworkSimulator_Struct_h
#define NetworkSimulator_Struct_h

class Option {};

typedef struct {
    int StartNodeId;
    int EndNodeId;
    double StartTime;
    double StopTime;
    int data;
    int id;
    Option *option;
    int StartID;
} ApplicationContainer;

typedef struct {
    double x;
    double y;
    double z;
} Vector;

typedef enum {
    ConstantPositionMobilityModel
} MobilityModel;

template <typename T,typename U>
class Tuple {
public:
    T _1;
    U _2;
    Tuple(T __1, U __2) {
        _1 = __1;
        _2 = __2;
    }
};

#endif
