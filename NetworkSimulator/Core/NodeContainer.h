//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/08.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__NodeContainer__
#define __NetworkSimulator__NodeContainer__

#include <vector>
#include "Node.h"
#include "delegate.h"
#include "Struct.h"
#include "Link.h"

using namespace std;

class NodeContainer {
    int _nodeNum;
    vector<Node> nodes;
public:
    void initNodes(int,Delegate<Mobility, int>,Delegate<double, int>);
    void initAC(int, Delegate<ApplicationContainer, int>);
    void initHello(double, int);
    bool existNodeEvent();
    Tuple<double, int> minNodeNextEvent();
    vector<Link> nodeEvent(int, double);
    
    void addEvent(Tuple<int, ApplicationContainer>, double);
    string toString();
};

#endif /* defined(__NetworkSimulator__File__) */
