//
//  File.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2013/01/08.
//  Copyright (c) 2013年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__LinkContainer__
#define __NetworkSimulator__LinkContainer__

#include <vector>
#include "Node.h"
#include "delegate.h"
#include "Struct.h"
#include "Link.h"

class LinkContainer {
    vector<Link> links;
public:
    void addEvent(vector<Link>);
    bool existLinkEvent();
    Tuple<double, int> minLinkNextEvent();
    bool existCollisionLink(int i);
    Tuple<int, ApplicationContainer> linkEvent(int i);
};
#endif /* defined(__NetworkSimulator__File__) */
