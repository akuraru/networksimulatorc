//
//  Node.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#include "Node.h"
#include "AHBP.h"
#include <sstream>

using namespace std;

bool compareAC(const ApplicationContainer& left, const ApplicationContainer& right)
{
    return left.StartTime < right.StartTime ;
}

Node::Node() {
    _mobility = Mobility();
    _transferRate = 1;
    eventList = vector<ApplicationContainer>();
    transTime = 0;
};
void Node::setNodeNum(int nn) {
    nodeNum = nn;
}
void Node::initHello(double interval, int size) {
    hp.init(interval, size);
}
void Node::setMobility(Mobility m) {
    _mobility = m;
}
void Node::setTransferRate(double tr) {
    _transferRate = tr;
}
double Node::TransferRate() {
    return _transferRate;
}
void Node::setEvent(ApplicationContainer ac, double transmission) {
    if (ac.id == -1) {
        Neighbor *n = (Neighbor *)ac.option;
        ns.setNeighbor(n);
        delete n;
    } else if (ac.StartNodeId < ac.EndNodeId && !existEvent(ac.id) && ((AHBP*)ac.option)->isDominator(nodeNum)) {
        ac.StartNodeId++;
        ac.StartTime += ((double)rand())/INT_MAX * transmission;
        setEvent(ac);
    } else {
        receiveList.push_back(ac);
        AHBP* o = (AHBP*)ac.option;
        delete o;
    }
}
void Node::addEvent(ApplicationContainer ac) {
    ac.option = (Option*)new AHBP();
    setEvent(ac);
}
void Node::setEvent(ApplicationContainer ac) {
    eventList.push_back(ac);
    sort(eventList.begin(), eventList.end(), compareAC);
}
ApplicationContainer Node::event() {
    ApplicationContainer ac;
    if (eventList.size() && eventList[0].StartTime < hp.nextTime()){
        ac = eventList[0];
        AHBP *o = (AHBP*)ac.option;
        o = new AHBP(o, ns.getNeighbor(), nodeNum);
        o->setNodeNum(nodeNum);
        ac.option = (Option*)o;
    } else {
        ac = hp.helloPacket();
        Neighbor *n = ns.getAdjacent();
        n->setNodeNum(nodeNum);
        ac.option = (Option*)n;
    }
    return ac;
}
void Node::stepEvent() {
    ApplicationContainer ac;
    if (eventList.size() && eventList[0].StartTime < hp.nextTime()){
        ac = eventList[0];
        delete (AHBP*)ac.option;
        deleteEvent();
    } else {
        ac = hp.helloPacket();
    }
    hp.helloDelay();
    delay(ac.data / _transferRate);
}
void Node::delay(double time) {
    for (int i = 0, _len = (int)eventList.size();  i < _len; i++) {
        eventList[i].StartTime += time;
    }
    hp.delay(time);
};
void Node::delay(double StartTime, double endTime) {
    double time = endTime - max(StartTime,transTime);
    transTime = max(transTime, endTime);
    
    if (time  <= 0) return;
    
    delay(time);
}
double Node::timeNextApplication() {
    if (eventList.size()) {
        ApplicationContainer ac = eventList[0];
        return min(ac.StartTime, hp.nextTime());
    } else {
        return hp.nextTime();
    }
}

bool Node::transfer(const Node& node,double maxDistance) {
    return _mobility.nearsight(node._mobility) <= maxDistance;
}
bool Node::existEvent(int id) {
    for (int i = 0, _len = (int)endEventList.size(); i < _len; i++) {
        if (endEventList[i].id == id)
            return true;
    }
    for (int i = 0, _len = (int)eventList.size(); i < _len; i++) {
        if (eventList[i].id == id)
            return true;
    }
    return false;
}
bool Node::existEvent() {
    return 0 < eventList.size();
}
void Node::deleteEvent() {
    endEventList.push_back(eventList[0]);
    eventList.erase(eventList.begin());
}

string Node::toString() {
    string str;
//    str += ns.toString();
    
    int _len = (int)endEventList.size();
    for (int i = 0; i < _len; i++) {
        str += ApptoString(endEventList[i]);
    }
    str += receiveToString();
    return str;
}

string Node::ApptoString(ApplicationContainer ac) {
    stringstream str;
    str << "transfer : " << ac.id << ", time : " << ac.StartTime << "\n";
    return str.str();
}
string Node::receiveToString() {
    stringstream str;
    for (int i=0,_len=(int)receiveList.size();i<_len;i++) {
        str << "receiver : " << receiveList[i].id << ", time : " << receiveList[i].StartTime << "\n";
    }
    return str.str();
}