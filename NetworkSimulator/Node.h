//
//  Node.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__Node__
#define __NetworkSimulator__Node__

#include "Mobility.h"
#include "vector"
#include "HelloPacket.h"
#include "NeighborSupporter.h"

using namespace std;

class Node {
    int nodeNum;
    
    Mobility _mobility;
    double _transferRate;
    double transTime;
    vector<ApplicationContainer> eventList;
    vector<ApplicationContainer> receiveList;
    vector<ApplicationContainer> endEventList;
    HelloPacket hp;
    NeighborSupporter ns;
    
    void deleteEvent();
    void delay(double);
public :
    Node();
    void initHello(double, int);
    void setNodeNum(int);
    void setMobility(Mobility);
    void setTransferRate(double);
    double TransferRate();
    void setEvent(ApplicationContainer, double);
    void setEvent(ApplicationContainer);
    void addEvent(ApplicationContainer);
    ApplicationContainer event();
    void stepEvent();
    
    void delay(double, double);
    
    double timeNextApplication();

    bool transfer(const Node&,double);
    bool existEvent(int);
    bool existEvent();
    
    string toString();
    string ApptoString(ApplicationContainer ac);
    string receiveToString();
};

#endif /* defined(__NetworkSimulator__Node__) */
