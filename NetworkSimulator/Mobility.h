//
//  Mobility.h
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#ifndef __NetworkSimulator__Mobility__
#define __NetworkSimulator__Mobility__

#include "Struct.h"

class Mobility {
    Vector _position;
    MobilityModel _model;
public:
    Mobility();
    Mobility(Vector p, MobilityModel mm);
    
    void setPosition(Vector p);
    Vector postion();
    
    void setModel(MobilityModel mm);
    MobilityModel model();
    
    double nearsight(Mobility);
};

#endif /* defined(__NetworkSimulator__Mobility__) */
