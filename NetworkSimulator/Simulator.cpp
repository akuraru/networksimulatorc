//
//  Simulator.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/21.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//

#include "Simulator.h"
#include "iostream"
#include <sstream>

using namespace std;


void Simulator::run() {
    srand((unsigned int)time(NULL));
    
    
    nc.initNodes(nodeNum(), Delegate<Mobility, int>(this, &Simulator::mobility), Delegate<double, int>(this, &Simulator::transferRate));
    nc.initAC(containerNum(), Delegate<ApplicationContainer, int>(this, &Simulator::applicationContainer));
    nc.initHello(helloInterval(), helloPacketSize());
    
    while (existEvent()) {
        Tuple<int, int> t = minNextEvent();
        (t._1 == 0)?
            lc.addEvent(nc.nodeEvent(t._2,transmissionDistance())) :
            nc.addEvent(lc.linkEvent(t._2), transmissionDelay());
    }
    
}
string Simulator::toString() {
    return nc.toString();
}


bool Simulator::existEvent() {
    return nc.existNodeEvent() || lc.existLinkEvent();
}

Tuple<int, int> Simulator::minNextEvent() {
    Tuple<double, int> mnne = nc.minNodeNextEvent();
    Tuple<double, int> mlne = lc.minLinkNextEvent();
    
    return (mnne._1 < mlne._1)? Tuple<int, int>(0, mnne._2) : Tuple<int, int>(1, mlne._2) ;
}
