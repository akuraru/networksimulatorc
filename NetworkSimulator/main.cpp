//
//  main.cpp
//  NetworkSimulator
//
//  Created by P.I.akura on 2012/12/20.
//  Copyright (c) 2012年 P.I.akura. All rights reserved.
//
#include "Core.h"
#include <iostream>
#include <math.h>

using namespace std;

class AkuraruSimulator : public Simulator {
    int nodeNum() override {
        return 25;
    }
    Mobility mobility(int id) override {
        int w = sqrt(nodeNum());
        return Mobility(Vector{(id%w)*5.0, (id/w)*5.0, 0.0}, ConstantPositionMobilityModel);
    }
    int dataRate() override {
        return 5400;
    };
    
    // 転送関連
    double transferRate(int nodeId) override {
        return 54000000;
    };
    double transmissionDistance() {
        return 8;
    }
    double transmissionDelay() {
        return 0.2;
    }
    int containerNum() override {
        return 2;
    }
    ApplicationContainer applicationContainer(int packectId) override {
        return {0, 100,100.0 + 1 * packectId , 250.0, dataRate(), packectId, NULL, 0};
    };
    
    // 隣接
    double helloInterval() {
        return 15;
    }
    int helloPacketSize() {
        return 1000;
    }
};

int main(int argc, const char * argv[])
{
    AkuraruSimulator anc;
    anc.run();
    cout << anc.toString() << "end" << endl;
}

